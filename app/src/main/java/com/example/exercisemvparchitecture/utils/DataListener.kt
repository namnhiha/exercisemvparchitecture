package com.example.exercisemvparchitecture.utils

interface DataListener<T>{
    fun onSuccess(data:T)
    fun onError(e:Exception)
}
