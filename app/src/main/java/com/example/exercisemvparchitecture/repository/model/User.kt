package com.example.exercisemvparchitecture.repository.model

data class User(var id: Int, var nameLogin: String, var pass: String, var age: String)
