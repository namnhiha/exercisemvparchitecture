package com.example.exercisemvparchitecture.repository.local

import android.os.Handler
import android.util.Log
import com.example.exercisemvparchitecture.repository.model.User
import com.example.exercisemvparchitecture.repository.RepositoryConstract
import com.example.exercisemvparchitecture.utils.DataListener

class LocalSource : RepositoryConstract.local {
    lateinit var mHandler: Handler
    override fun loadUser(listener: DataListener<User>) {
        Log.i("TAG", "Toi day chua")
//        mHandler =
//            object : Handler(Looper.getMainLooper()) {
//                override fun handleMessage(msg: Message) {
//                    Log.i("TAG", "s")
//                    when (msg.what) {
//                        1 -> {
//                            listener.onSuccess(msg.obj as User)
//                        }
//                    }
//                }
//            }
        //Giả sử tại đây dữ liệu mất tới 7s mới trả về.
        Thread {
            Thread.sleep(7000)
//            val message = Message()
//            message.what = 1
//            message.obj = User(1, "Tuan", "123456", "44")
//            mHandler.sendMessage(message)
//            Log.i("TAG", Thread.currentThread().toString())
            listener.onSuccess(User(1, "Tuan", "123456", "44"))
        }.start()
    }

    companion object {
        fun newInstance() = LocalSource()
    }
}
