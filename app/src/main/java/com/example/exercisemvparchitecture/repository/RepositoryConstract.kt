package com.example.exercisemvparchitecture.repository

import com.example.exercisemvparchitecture.repository.model.User
import com.example.exercisemvparchitecture.utils.DataListener

interface RepositoryConstract {
    interface remote
    interface local {
        fun loadUser(listener: DataListener<User>)
    }
}
