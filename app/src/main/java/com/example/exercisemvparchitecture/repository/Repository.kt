package com.example.exercisemvparchitecture.repository

import com.example.exercisemvparchitecture.repository.model.User
import com.example.exercisemvparchitecture.utils.DataListener

class Repository(
    private var local: RepositoryConstract.local,
    private var remote: RepositoryConstract.remote
) {
    fun loadUser(listener: DataListener<User>) {
        local.loadUser(listener)
    }

    companion object {
        @Synchronized
        fun newInstance(
            local: RepositoryConstract.local,
            remote: RepositoryConstract.remote
        ): Repository = Repository(local, remote)
    }
}
