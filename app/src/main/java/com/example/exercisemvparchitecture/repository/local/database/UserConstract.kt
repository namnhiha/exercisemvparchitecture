package com.example.exercisemvparchitecture.repository.local.database

import android.provider.BaseColumns

object UserContract {
    object UserEntry : BaseColumns {
        const val TABLE_NAME = "User"
        const val COLUMN_NAME_NAME = "name"
        const val COLUMN_NAME_PASS = "pass"
        const val COLUMN_NAME_AGE = "age"
    }
}
