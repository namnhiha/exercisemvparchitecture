package com.example.exercisemvparchitecture.view.login

import com.example.exercisemvparchitecture.repository.model.User

interface LoginView {
    fun onSuccess(user: User)
    fun onError(error:String)
}
