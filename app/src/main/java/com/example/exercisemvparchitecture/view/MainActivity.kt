package com.example.exercisemvparchitecture.view

import com.example.exercisemvparchitecture.R
import com.example.exercisemvparchitecture.base.BaseActivity
import com.example.exercisemvparchitecture.view.login.LoginFragment

class MainActivity : BaseActivity() {
    override fun getLayout() = R.layout.activity_main
    override fun onResume() {
        super.onResume()
        supportFragmentManager.beginTransaction().add(R.id.frameLayout, LoginFragment.newInstance())
            .commit()
    }
}
