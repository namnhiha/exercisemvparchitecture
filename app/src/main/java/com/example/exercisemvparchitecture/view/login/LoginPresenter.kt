package com.example.exercisemvparchitecture.view.login

import com.example.exercisemvparchitecture.repository.model.User
import com.example.exercisemvparchitecture.repository.Repository
import com.example.exercisemvparchitecture.utils.DataListener

class LoginPresenter(private var mRepository: Repository) {
    private var mLoginView: LoginView? = null

    fun setView(loginView: LoginView) {
        mLoginView = loginView
    }

    fun loadUser() {
        mRepository.loadUser(object : DataListener<User> {
            override fun onSuccess(data: User) {
                mLoginView?.onSuccess(data)
            }

            override fun onError(e: Exception) {
                mLoginView?.onError(e.toString())
            }
        })
    }
}
