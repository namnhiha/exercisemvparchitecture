package com.example.exercisemvparchitecture.view.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.exercisemvparchitecture.R
import com.example.exercisemvparchitecture.repository.model.User
import com.example.exercisemvparchitecture.repository.Repository
import com.example.exercisemvparchitecture.repository.local.LocalSource
import com.example.exercisemvparchitecture.repository.remote.RemoteSource
import kotlinx.android.synthetic.main.fragment1.*

class LoginFragment : Fragment(), View.OnClickListener, LoginView {
    private val mLoginPresenter =
        LoginPresenter(
            Repository.newInstance(
                LocalSource.newInstance(),
                RemoteSource.newInstance()
            )
        )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment1, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mLoginPresenter.setView(this)
    }

    override fun onResume() {
        super.onResume()
        buttonLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.buttonLogin -> {
                mLoginPresenter.loadUser()
            }
        }
    }

    override fun onSuccess(user: User) {
        Log.i("TAG", user.toString())
    }

    override fun onError(error: String) {
        Log.i("TAG", error)
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}
